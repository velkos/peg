import {Visitor} from './Visitor.mjs';

export const defineValue = ({visitor = new Visitor(), parent, property, definitions}) => {
	if(!visitor.visited(parent)) {
		const VALUE = parent[property];
		if(definitions.hasOwnProperty(VALUE)) {
			const DEF = definitions[VALUE];
			if(DEF && DEF.define) {
				DEF.define(definitions, visitor);
			}
		}
		else if(VALUE && VALUE.define) {
			VALUE.define(definitions, visitor);
		}
		visitor.visit(parent);
	}
};