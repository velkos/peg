export class Context {
	constructor({text, offset = 0, _cache = null}) {
		this.text = text;
		this.offset = offset;
		this._cache = _cache;
	}

	advance(n) {
		this.text = this.text.slice(n);
		this.offset += n;
		return this;
	}

	clone() {
		return new Context(this);
	}
};