import {Visitor} from './Visitor.mjs';

export const defineElement = ({visitor = new Visitor(), parent, destination, definitions}) => {
	if(!visitor.visited(parent)) {
		let offset = 0;
		destination.slice().forEach((parser, index) => {
			if(!visitor.visited(parser)) {
				if(definitions.hasOwnProperty(parser)) {
					const DEF = definitions[parser];
					if(Array.isArray(DEF)) {
						destination.splice(index + offset, 1, ...DEF);
						offset += DEF.length;
						DEF.forEach(subDef => {
							if(subDef.define) {
								subDef.define(definitions, visitor);
							}
						});
					}
					else if(DEF === null) {
						destination.slice(index + offset--, 1);
					}
					else {
						destination[index + offset] = DEF;
						if(DEF && DEF.define) {
							DEF.define(definitions, visitor);
						}
					}
				}
				else if(parser.define) {
					parser.define(definitions, visitor);
				}
			} 
		});
		visitor.visit(parent);
	}
};