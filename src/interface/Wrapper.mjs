import {Parser} from './Parser.mjs';
import {Visitor} from '../Visitor.mjs';
import {defineValue} from '../defineValue.mjs'

export class Wrapper extends Parser {
	constructor({_parser, ...rest}) {
		super(rest);
		this._parser = _parser;
	}

	define(definitions, visitor = new Visitor()) {
		defineValue({
			visitor,
			parent: this,
			property: '_parser',
			definitions
		});
		return this;
	}
};