import {Parser} from './Parser.mjs';
import {Visitor} from '../Visitor.mjs';
import {quoteError} from '../quoteError.mjs';
import {defineElement} from '../defineElement.mjs';

const HANDLER = function({index, error}) {
		return `${this.constructor.name} failed because parser at index ${index} failed with error: ${quoteError(error)}.`
	};

export class Combinator extends Parser {
	constructor({_parsers, ...rest}) {
		super({_handler: HANDLER, ...rest});
		this._parsers = _parsers;
	}

	define(definitions, visitor) {
		defineElement({
			visitor,
			parent: this,
			destination: this._parsers,
			definitions
		});
		return this;
	}
};