import {Cache} from '../Cache.mjs';
import {Context} from '../Context.mjs';
import {unimplemented} from '../unimplemented.mjs';

const NORMALIZE_CONTEXT = context => {
		switch(typeof context) {
			case 'object':
				if(context instanceof Context) {
					return context;
				}
				else {
					return new Context({
						text,
						_cache: context.cached ? new Cache() : null
					});
				}
			case 'string':
				return new Context({
					text: context,
					_cache: new Cache()
				});
			default:
				throw new Error('Unknown context.');
		}
	},
	CACHE_AND_RETURN = ({cache, cacheInfo, result}) => {
		cacheInfo.result = result;
		cache.set(cacheInfo);
		return result;
	};

export class Parser {
	constructor({_mapper = null, _handler = null, _cacheable = true} = {}) {
		this._mapper = _mapper;
		this._handler = _handler;
		this._cacheable = _cacheable;
	}

	handle(handler) {
		this._handler = handler;
		return this;
	}

	map(mapper) {
		this._mapper = mapper;
		return this;
	}

	clone() {
		return new this.constructor(this);
	}

	_parse(context) {
		unimplemented({method: '_parse', type: this.constructor.name});
	}

	parse(context) {
		const CONTEXT = NORMALIZE_CONTEXT(context),
			{_cache: CACHE, offset: OFFSET} = CONTEXT;
		if(this._cacheable && CACHE) {
			const CACHE_INFO = {
					rule: this,
					offset: OFFSET
				};
			if(CACHE.has(CACHE_INFO)) {
				return CACHE.get(CACHE_INFO);
			}
			else {
				return this._parse(CONTEXT)
					.then(
						result => {
							return CACHE_AND_RETURN({
								cache: CACHE,
								cacheInfo: CACHE_INFO,
								result: Promise.resolve({
									length: result.length,
									match: this._mapper ? this._mapper(result) : result.match
								})
							});
						},
						error => {
							return CACHE_AND_RETURN({
								cache: CACHE,
								cacheInfo: CACHE_INFO,
								result: Promise.reject(this._handler ? this._handler(error) : error)
							});
						}
					);
			}
		}
		else {
			return this._parse(CONTEXT);
		}
	}
};