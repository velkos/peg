import {Parser} from '../interface/Parser.mjs';
import {mismatchIndex} from '../mismatchIndex.mjs';

const HANDLE = ({expected, found, offset}) => `Expected '${expected}' but found '${found}' at offset ${offset}.`;

export class String extends Parser {
	constructor({_string, ...rest}) {
		super({_handler: HANDLE, ...rest});
		this._string = _string;
	}

	_parse({text, offset}) {
		const STRING = this._string,
			LEN = STRING.length,
			INDEX = mismatchIndex({text, string: STRING});
		return INDEX >= LEN ?
			Promise.resolve({
				match: STRING,
				length: LEN,
				offset
			}) :
			Promise.reject({
				expected: STRING.charAt(INDEX),
				found: text.charAt(INDEX),
				offset: offset + INDEX
			});
	}
};