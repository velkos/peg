import {Parser} from '../interface/Parser.mjs';

const HANDLE = ({character, offset}) => `The character '${character}' at offset ${offset} did not pass the provided function.`;

export class CharacterPredicate extends Parser {
	constructor({_predicate, ...rest}) {
		super({_handler: HANDLE, ...rest});
		this._predicate = _predicate;
	}

	_parse({text, offset}) {
		const CHAR = text.charAt(0);
		return CHAR && this._predicate(CHAR) ?
			Promise.resolve({
				match: CHAR,
				length: 1,
				offset
			}) :
			Promise.reject({
				character: CHAR,
				offset
			});
	}
};