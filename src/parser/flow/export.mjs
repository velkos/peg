import {Range} from './Range.mjs';

export * from './optional.mjs';
export * from './zeroOrMore.mjs';
export * from './oneOrMore.mjs';
export const range = (_min, _max) => {
	return _parser => new Range({_min, _max, _parser});
};