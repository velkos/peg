import {Range} from './Range.mjs';

export const zeroOrMore = _parser => {
	return new Range({
		_min: 0,
		_max: Infinity,
		_parser
	});
};