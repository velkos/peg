import {Range} from './Range.mjs';

export const optional = _parser => {
	return new Range({
		_min: 0,
		_max: 1,
		_parser
	});
};