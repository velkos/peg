import {Wrapper} from '../../interface/Wrapper.mjs';
import {quoteError} from '../../quoteError.mjs';

const HANDLE = ({iteration, error}) => `Range failed at iteration ${iteration} with error: ${quoteError(error)}.`;

export class Range extends Wrapper {
	constructor({_min = 0, _max = 0, ...rest} = {}) {
		super({_handler: HANDLE, ...rest});
		this._min = _min;
		this._max = _max;
	}

	_parse(context) {
		let i = 0,
			error;
		const CONTEXT = context.clone(),
			START_OFFSET = context.offset,
			MAX = this._max,
			MATCHES = [],
			PARSER = this._parser,
			NEXT = () => {
				if(i < MAX) {
					return PARSER.parse(CONTEXT).then(ON_SUCCESS, ON_FAILURE);
				}
				else {
					return LAST();
				}
			},
			LAST = () => {
				return Promise.resolve({
					match: MATCHES,
					length: CONTEXT.offset - START_OFFSET,
					offset: START_OFFSET
				});
			},
			ON_SUCCESS = result => {
				const {match: MATCH, length: LEN} = result;
				MATCHES.push(MATCH);
				CONTEXT.advance(LEN);
				++i;
				return NEXT();
			},
			ON_FAILURE = error => {
				if(i >= this._min) {
					return LAST();
				}
				else {
					return Promise.reject({
						iteration: i,
						error
					});
				}
			};
		return NEXT();
	}
};