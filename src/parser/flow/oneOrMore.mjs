import {Range} from './Range.mjs';

export const oneOrMore = _parser => {
	return new Range({
		_min: 1,
		_max: Infinity,
		_parser
	});
};