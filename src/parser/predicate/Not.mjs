import {Wrapper} from '../../interface/Wrapper.mjs';

const HANDLE = value => `Not predicate failed because the nested parser succeeded with value ${value}.`;

export class Not extends Wrapper {
	constructor(args) {
		super(args);
	}

	_parse(context) {
		return this._parser.parse(context)
			.then(
				result => {
					return Promise.reject(result.match);
				},
				error => {
					return Promise.resolve({
						match: undefined,
						length: 0
					});
				}
			);
	}
};