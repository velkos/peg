import {And} from './And.mjs';
import {Not} from './Not.mjs';

export const and = _parser => new And({_parser}),
	not = _parser => new Not({_parser});