import {Wrapper} from '../../interface/Wrapper.mjs';
import {quoteError} from '../../quoteError.mjs';

const HANDLE = ({offset, error}) => `And predicate failed at offset ${offset} because the nested parser failed with error: ${quoteError(error)}.`;

export class And extends Wrapper {
	constructor(args) {
		super({_handler: HANDLE, ...args});
	}

	_parse(context) {
		return this._parser.parse(context)
			.then(
				result => {
					return {
						match: result.match,
						length: 0
					};
				},
				error => {
					return {
						offset: context.offset,
						error
					};
				}
			);
	}
};