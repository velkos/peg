import {Combinator} from '../../interface/Combinator.mjs';

export class Choice extends Combinator {
	constructor(args) {
		super(args);
	}

	_parse(context) {
		let i = 0;
		const PARSERS = this._parsers,
			LAST = PARSERS.length - 1,
			NEXT = () => {
				return PARSERS[i].parse(context).then(ON_SUCCESS, ON_FAILURE);
			},
			ON_SUCCESS = result => {
				return Promise.resolve({
					index: i,
					match: result.match,
					offset: context.offset,
					length: result.length
				});
			},
			ON_FAILURE = error => {
				if(i !== LAST) {
					++i;
					return NEXT();
				}
				else {
					return Promise.reject({
						index: i,
						error
					});
				}
			};
		return NEXT();
	}
};