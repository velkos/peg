import {Combinator} from '../../interface/Combinator.mjs';

export class Sequence extends Combinator {
	constructor(args) {
		super(args);
	}

	_parse(context) {
		let i = 0;
		const PARSERS = this._parsers,
			LEN = PARSERS.length,
			MATCHES = [],
			START_OFFSET = context.offset,
			CONTEXT = context.clone(),
			NEXT = () => {
				return PARSERS[i].parse(CONTEXT).then(ON_SUCCESS, ON_FAILURE);
			},
			ON_SUCCESS = result => {
				MATCHES.push(result.match);
				CONTEXT.advance(result.length);
				if(++i < LEN) {
					return NEXT();
				}
				else {
					return Promise.resolve({
						match: MATCHES,
						offset: START_OFFSET,
						length: CONTEXT.offset - START_OFFSET,
					});
				}
			},
			ON_FAILURE = error => {
				return Promise.reject({
					error,
					index: i,
					offset: CONTEXT.offset
				});
			};
		return NEXT();
	}
};