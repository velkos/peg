import {Choice} from './Choice.mjs';
import {Sequence} from './Sequence.mjs';

export const choice = (..._parsers) => new Choice({_parsers}),
	sequence = (..._parsers) => new Sequence({_parsers});