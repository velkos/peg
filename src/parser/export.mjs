import {String} from './String.mjs';
import {CharacterPredicate} from './CharacterPredicate.mjs';

export const string = _string => new String({_string}),
	characterPredicate = _predicate => new CharacterPredicate({_predicate});

export * from './combinator/export.mjs';
export * from './flow/export.mjs';
export * from './predicate/export.mjs';