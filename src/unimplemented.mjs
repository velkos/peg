export const unimplemented = ({method, type}) => {
	throw new Error(`Method "${method}" lacks implementation in class "${type}".`);
};