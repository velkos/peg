export class Visitor {
	constructor() {
		this._visited = [];
	}

	visited(x) {
		return this._visited.indexOf(x) !== -1;
	}

	visit(x) {
		if(!this.visited(x)) {
			this._visited.push(x);
		}
		return this;
	}
};