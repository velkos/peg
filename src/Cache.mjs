export class Cache {
	constructor() {
		this._rules = new WeakMap();
	}

	set({rule, offset, result}) {
		const MAP = this._rules;
		if(MAP.has(rule)) {
			const RULE = MAP.get(rule);
			RULE[offset] = result;
		}
		else {
			MAP.set(
				rule,
				{
					[offset]: result
				}
			);
		}
		return this;
	}

	has({rule, offset}) {
		const RULE = this._rules.get(rule);
		return !!(RULE && RULE.hasOwnProperty(offset));
	}

	get({rule, offset}) {
		const RULE = this._rules.get(rule);
		if(RULE) {
			return RULE[offset];
		}
	}
}